using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Tools;
using Thingie.WPF.Controls.PropertiesEditor;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
    public interface IValidatable
    {
        ValidationResult Validate();
    }

    public class WindowService
    {
        private readonly IDialogService dialogService;

        public WindowService(IDialogService dialogService)
    	{
            this.dialogService = dialogService;
        }
    
    	// todo: move to DialogService and make two versions
    	// - make it return true/false if it's shown as model i.e. ShowDialog()
    	// - make it accept a save-action if it's shown as non-model i.e. Show()
    
        public bool DisplayObject(string title, object target)
        {
            Window dialog = new Window();
            dialog.Title = title;

            PropertiesEditorUC editor = new PropertiesEditorUC();
            editor.AutoCommit = true;
            editor.Target = target;

            var okButton = new System.Windows.Controls.Button();
            okButton.Click += (s, e) =>
            {
                if (target is IValidatable v)
                {
                	var res = v.Validate();
                	if(!res.IsValid)
                	{
                		MessageBox.Show($"{res.ErrorContent}", "Invalid data");
                		return;
                	}
                }

                dialog.DialogResult = true; 
                dialog.Close();
            };
            okButton.HorizontalAlignment = HorizontalAlignment.Right;
            okButton.Margin = new Thickness(10);
            okButton.SetValue(System.Windows.Controls.DockPanel.DockProperty, System.Windows.Controls.Dock.Bottom);
            okButton.MinWidth = 100;
            okButton.Content = "OK";

            var dockpanel = new System.Windows.Controls.DockPanel();
            dockpanel.LastChildFill = true;
            dockpanel.Children.Add(okButton);
            dockpanel.Children.Add(editor);

            dialog.Height = 450;
            dialog.Width = 600;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            dialog.Content = dockpanel;
            return dialogService.ShowWindowDialog(dialog) == true;
        }
    }
}
