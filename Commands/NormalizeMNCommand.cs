using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Humanizer;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Tools;
using Thingie.WPF;
using Thingie.WPF.Attributes;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools.Commands
{
    public class ExtractLookupMNParameters : ChangeNotifierBase, IValidatable
    {
        public ExtractLookupMNParameters(string tableName, string columnName, IEnumerable<string> allColumnsList)
        {
            TableName = tableName;
            ColumnName = columnName;
            AllColumnsList = allColumnsList;
        }

        [Viewable, Category("Source data"), Order(1)]
        public string TableName { get; }
        [Viewable, Category("Source data"), Order(2)]
        public string ColumnName { get; }
        public IEnumerable<string> AllColumnsList { get; }
        [EditableChoice(nameof(AllColumnsList)), Category("Source data"), Order(3)]
        public string IdColumn { get; set; }

        [Editable, Category("Source data"), Order(4)]
        public bool RemoveSourceColumn { get; set; }

        [Editable, Order(11)]
        public string Delimiter { get; set; } = ",";

        [Editable, Category("Lookup table"), Order(21)]
        public string ExtractedTableName { get; set; }
        [Editable, Category("Lookup table"), Order(22)]
        public string ExtractedValuesColumnName { get; set; }

        public ValidationResult Validate()
        {
            return ValidationResult.ValidResult;
        }
    }

    public class NormalizeMNCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;
        private readonly WindowService windowService;
        private readonly IQueryStormLogger logger;

        public NormalizeMNCommand(IExcelAccessor excelAccessor, WindowService windowService, IQueryStormLogger logger)
            : base("many-to-many", new[] { KnownContextMenuLocations.Table }, "Normalize", 1)
        {
            this.excelAccessor = excelAccessor;
            this.windowService = windowService;
            this.logger = logger;
        }

        public override void Execute()
        {
            var excel = excelAccessor.Application;
            var selectedRange = excel.Selection as Microsoft.Office.Interop.Excel.Range;

            var tableSelection = Tools.GetTableSelection(selectedRange);
            var table = tableSelection.ListObject;
            var headers = tableSelection.Columns;

            TryGetParameters(tableSelection).Do(cfg =>
            {
                var tabular = new TabularListObject(table);
                var tables = tabular.NormalizeMN(cfg.IdColumn, cfg.ColumnName, cfg.Delimiter, cfg.ExtractedTableName, cfg.ExtractedValuesColumnName);

                var workbook = excel.ActiveWorkbook;
                var afterWorksheet = workbook.ActiveSheet as Microsoft.Office.Interop.Excel.Worksheet;

                var lookupSheet = workbook.Sheets.Add(After: afterWorksheet) as Microsoft.Office.Interop.Excel.Worksheet;
                lookupSheet.Name = tables.ExtractedTable.Name;
                (lookupSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).WriteTable(tables.ExtractedTable, tables.ExtractedTable.Name);
                afterWorksheet = lookupSheet;

                var linkSheet = workbook.Sheets.Add(After: afterWorksheet) as Microsoft.Office.Interop.Excel.Worksheet;
                linkSheet.Name = tables.LinkTable.Name;
                (linkSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).WriteTable(tables.LinkTable, tables.LinkTable.Name);

                if (cfg.RemoveSourceColumn)
                    table.ListColumns[cfg.ColumnName].Delete();
            });
        }


        private Option<ExtractLookupMNParameters> TryGetParameters(TableSelection selection)
        {
            var cfg = new ExtractLookupMNParameters(selection.ListObject.Name, selection.Columns.First(), selection.ListObject.ListColumns.OfType<Microsoft.Office.Interop.Excel.ListColumn>().Select(c => c.Name).ToList());

            cfg.ExtractedTableName = selection.Columns.First().Singularize();
            cfg.ExtractedValuesColumnName = "Name";
            cfg.IdColumn = cfg.AllColumnsList.Where(c => c == "Id").SingleOrDefault() ??
                cfg.AllColumnsList.Where(c => c.ToLower().EndsWith("id")).FirstOrDefault() ??
                cfg.AllColumnsList.First();

            var res = windowService.DisplayObject("Extract-Lookup parameters", cfg);
            if (res != true)
                return None.Value;
            else
                return cfg;
        }
    }
}
