using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using Humanizer;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using Thingie.WPF;
using Thingie.WPF.Attributes;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;

namespace Windy.TableTools
{
    public class ExtractLookupParameters : ChangeNotifierBase, IValidatable
    {
        private bool putLookupTableInNewSheet = true;
        private DetailsTableDestination detailsTableDestination = DetailsTableDestination.InNewSheet;

        [Viewable, Category("Source data"), Order(1)]
        public string TableName { get; }
        [Viewable, Category("Source data"), Order(2)]
        public string ColumnNames { get; set; }

        public ExtractLookupParameters(string tableName)
        {
            TableName = tableName;
        }

        [Editable, Category("Lookup table"), Order(10)]
        public bool PutLookupTableInNewSheet
        {
            get => putLookupTableInNewSheet;
            set
            {
                putLookupTableInNewSheet = value;
                OnPropertyChanged(nameof(PutLookupTableInNewSheet));
            }
        }

        [Editable(When = "!PutLookupTableInNewSheet"), Category("Lookup table"), Order(11)]
        public string LookupTableDestinationAddress { get; set; }

        [Editable, Category("Lookup table"), Order(12)]
        public string LookupTableName { get; set; }

        [Editable, Category("Details table"), Order(21)]
        public DetailsTableDestination DetailsTableDestination
        {
            get => detailsTableDestination;
            set
            {
                detailsTableDestination = value;
                OnPropertyChanged(nameof(DetailsTableDestination));
                OnPropertyChanged(nameof(CanEnterAddress));
                OnPropertyChanged(nameof(IsCreatingNewDetailsTable));
            }
        }

        public bool CanEnterAddress => DetailsTableDestination == DetailsTableDestination.ManualLocation;
        public bool IsCreatingNewDetailsTable => DetailsTableDestination != DetailsTableDestination.ReplaceOriginalTable;

        [Editable(When = "CanEnterAddress"), Category("Details table"), Order(22)]
        public string DetailsTableDestinationAddress { get; set; }

        [Editable(When = "IsCreatingNewDetailsTable"), Category("Details table"), Order(23)]
        public string DetailsTableName { get; set; }

        [Editable, Category("Details table"), Order(24)]
        public string KeyColumnName { get; set; }

        public ValidationResult Validate()
        {
            if (string.IsNullOrEmpty(DetailsTableName) && DetailsTableDestination != DetailsTableDestination.ReplaceOriginalTable)
                return new ValidationResult(false, "Must set details destination table name if not replacing existing table!");
            return ValidationResult.ValidResult;
        }
    }

    public class Normalize1NCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;
        private readonly WindowService windowService;
        private readonly IQueryStormLogger logger;

        public Normalize1NCommand(IExcelAccessor excel, WindowService windowService, IQueryStormLogger logger)
            : base("one-to-many", new[] { KnownContextMenuLocations.Table }, "Normalize", 1)
        {
            this.excelAccessor = excel;
            this.windowService = windowService;
            this.logger = logger;
        }

        private Option<ExtractLookupParameters> TryGetParameters1N(TableSelection selection)
        {
            var cfg = new ExtractLookupParameters(selection.ListObject.Name);
            string lookupNameStrRoot = string.Join("", selection.Columns.Select(h => CultureInfo.InvariantCulture.TextInfo.ToTitleCase(h)));
            cfg.ColumnNames = string.Join(", ", selection.Columns);
            cfg.LookupTableName = lookupNameStrRoot.Pluralize();
            cfg.KeyColumnName = $"{lookupNameStrRoot}Id";
            cfg.DetailsTableName = selection.ListObject.Name + "(Normalized)";

            var res = windowService.DisplayObject("Extract-Lookup parameters", cfg);
            if (res != true)
                return None.Value;
            else
                return cfg;
        }

        public override void Execute()
        {
            try
            {
                var excel = excelAccessor.Application;
                var selectedRange = excel.Selection as Microsoft.Office.Interop.Excel.Range;

                var tableSelection = Tools.GetTableSelection(selectedRange);
                var table = tableSelection.ListObject;
                var headers = tableSelection.Columns;

                TryGetParameters1N(tableSelection).Do(cfg =>
                {
                    var tabular = new TabularListObject(table);
                    var tables = tabular.Normalize(headers, cfg.LookupTableName, cfg.DetailsTableName, cfg.KeyColumnName);

                    var workbook = excel.ActiveWorkbook;
                    var afterWorksheet = workbook.ActiveSheet as Microsoft.Office.Interop.Excel.Worksheet;

                    if (cfg.PutLookupTableInNewSheet)
                    {
                        var lookupSheet = workbook.Sheets.Add(After: afterWorksheet) as Microsoft.Office.Interop.Excel.Worksheet;
                        lookupSheet.Name = tables.Lookup.Name;
                        (lookupSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).WriteTable(tables.Lookup, tables.Lookup.Name);
                        afterWorksheet = lookupSheet;
                    }
                    else
                    {
                        workbook.GetRange(cfg.LookupTableDestinationAddress).WriteTable(tables.Lookup, tables.Lookup.Name);
                    }

                    if (cfg.DetailsTableDestination == DetailsTableDestination.InNewSheet)
                    {
                        var detailsSheet = workbook.Sheets.Add(After: afterWorksheet) as Microsoft.Office.Interop.Excel.Worksheet;
                        detailsSheet.Name = tables.Detail.Name;
                        (detailsSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).WriteTable(tables.Detail, tables.Detail.Name);
                    }
                    else if (cfg.DetailsTableDestination == DetailsTableDestination.ManualLocation)
                    {
                        workbook.GetRange(cfg.DetailsTableDestinationAddress).WriteTable(tables.Detail, tables.Detail.Name);
                    }
                    else if (cfg.DetailsTableDestination == DetailsTableDestination.ReplaceOriginalTable)
                    {
                        headers.ForEach(h => table.ListColumns[h].Delete());
                        var idCol = table.ListColumns.Add();
                        idCol.Name = cfg.KeyColumnName;
                        table.UpdateData(tables.Detail);
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
    }
}