using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Tools;
using Thingie.WPF;
using Thingie.WPF.Controls.PropertiesEditor.DefaultFactory.Attributes;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools.Commands
{
    public class OutliersParameters : ChangeNotifierBase
    {
        [Viewable]
        public string TargetTable { get; }

        [EditableChoice(nameof(ColumnNames))]
        public string TargetColumn { get; set; }

        [EditableChoice(nameof(ColumnNames))]
        public string GroupByColumn { get; set; }

        [Editable]
        public double MinDeviation { get; set; } = 2;

        public OutliersParameters(string targetTable, IEnumerable<string> columnNames)
        {
            TargetTable = targetTable;
            ColumnNames = columnNames;
        }

        public IEnumerable<string> ColumnNames { get; set; }
    }

    public class FindOutliersCommand : ContextMenuCommand
    {
        private readonly IExcelAccessor excelAccessor;
        private readonly WindowService windowService;
        private readonly IQueryStormLogger logger;
        private readonly IDialogService dialogService;

        public FindOutliersCommand(IExcelAccessor excelAccessor, WindowService windowService, IQueryStormLogger logger, IDialogService dialogService)
            : base("Find outliers", new[] { KnownContextMenuLocations.Table }, "Analyze", 1)
        {
            this.excelAccessor = excelAccessor;
            this.windowService = windowService;
            this.logger = logger;
            this.dialogService = dialogService;
        }

        public override void Execute()
        {
            try
            {
                var excel = excelAccessor.Application;
                var selectedRange = excel.Selection as Microsoft.Office.Interop.Excel.Range;

                var tableSelection = Tools.GetTableSelection(selectedRange);
                var table = tableSelection.ListObject;
                var headers = tableSelection.Columns;

                TryGetParametersOutliers(tableSelection).Do(cfg =>
                {
                    var tabular = new TabularListObject(table);
                    var result = tabular.FindOutliers(cfg.TargetColumn, cfg.GroupByColumn, cfg.MinDeviation);

                    if (result == null || result.Count() == 0)
                    {
                    	dialogService.ShowInfo("No outliers found!", "Info");
                    }
                    else
                    {
                        var workbook = excel.ActiveWorkbook;
                        var afterWorksheet = workbook.ActiveSheet as Microsoft.Office.Interop.Excel.Worksheet;
                        var lookupSheet = workbook.Sheets.Add(After: afterWorksheet) as Microsoft.Office.Interop.Excel.Worksheet;
                        lookupSheet.Name = result.Name;
                        (lookupSheet.Cells[1, 1] as Microsoft.Office.Interop.Excel.Range).WriteTable(result, result.Name);
                        afterWorksheet = lookupSheet;
                    }
                });
            }
            catch (Exception ex)
            {
                logger.Error("Failed to run outliers handler", ex);
            }
        }

        private Option<OutliersParameters> TryGetParametersOutliers(TableSelection selection)
        {
            var cfg = new OutliersParameters(selection.ListObject.Name, selection.ListObject.ListColumns.OfType<Microsoft.Office.Interop.Excel.ListColumn>().Select(c => c.Name).ToList());

            cfg.TargetColumn = selection.Columns.First();
            cfg.GroupByColumn = selection.Columns.ElementAtOrDefault(1);

            var res = windowService.DisplayObject("Find outliers", cfg);
            if (res != true)
                return None.Value;
            else
                return cfg;
        }
    }
}
