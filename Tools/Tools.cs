using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using QueryStorm.Apps;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
	public class TableSelection
	{
		public TableSelection(Microsoft.Office.Interop.Excel.ListObject listObject, IEnumerable<string> columns)
		{
            ListObject = listObject;
            Columns = columns;
        }

        public ListObject ListObject { get; }
        public IEnumerable<string> Columns { get; }
    }

	public class Tools
	{
		// todo: move to RangeExtensions
		public static TableSelection GetTableSelection(Range range)
		{
            var table = range.ListObject;

            var intersection = range.Application.Intersect(table.Range, range);
            List<string> headers = new List<string>();
            foreach (Microsoft.Office.Interop.Excel.Range r in intersection.Columns)
                headers.Add(table.ListColumns[r.Column - table.Range.Column + 1].Name);
                
            return new TableSelection(table, headers);
		}
	}
}
