using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Data;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
    public static partial class NormalizerExtensionMethods
    {
        public static MasterDetailNormalizationResult Normalize(this Tabular t, IEnumerable<string> columnNames, string masterTableName, string detailTableName, string relationColumnName)
        {
            var grouped = t
                .Select(r => new { row = r, x = columnNames.Select(cn => r[cn]) })
                .GroupBy(r => r.x, new ArrayValuesComparer())
                .Select((g, i) => new { id = i + 1, key = g.Key, rows = g.AsEnumerable() });

            var masterTbl = new TabularListOfArrays(
                masterTableName,
                new[] { "Id" }.Concat(columnNames),
                grouped.Select(g => new object[] { g.id }.Concat(g.key)));
			
            var table2columnNames = t.Columns.Where(c => !c.IsHidden).Select(c => c.Name).Except(columnNames);
            
            var tbl2data = grouped.SelectMany(g => g.rows.Select(r => table2columnNames.Select(cn => (object)r.row[cn]).Concat(new object[] { g.id })));
            
            var detailTbl = new TabularListOfArrays(
                detailTableName,
                table2columnNames.Concat(new[] { relationColumnName }),
                tbl2data);

            return new MasterDetailNormalizationResult (masterTbl, detailTbl);
        }
    }

    public class MasterDetailNormalizationResult
    {
        public MasterDetailNormalizationResult(Tabular master, Tabular detail)
        {
            Lookup = master;
            Detail = detail;
        }

        public Tabular Lookup { get; }
        public Tabular Detail { get; }
    }
}
