using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Data;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
	public class NormalizationMNResult
	{
		public NormalizationMNResult(Tabular extractedTable, Tabular linkTable)
		{
            ExtractedTable = extractedTable;
            LinkTable = linkTable;
        }

        public Tabular ExtractedTable { get; }
        public Tabular LinkTable { get; }
    }

	public static class NormalizeMNExtensionMethod
	{
		public static NormalizationMNResult NormalizeMN(this Tabular t, string idColumnName, string columnName, string delimiter, string extractedTableName, string extractedColumnName)
		{
			var grouped = t.SelectMany((r,i) => (r[columnName]?.ToString() ?? "").Split(new string [] { delimiter }, StringSplitOptions.None).Select(el => new { element = el, row = r, rowId = r[idColumnName] }))
				.GroupBy(x => x.element)
				.Select((g, i) => new {id = i+1, element = g.Key, rows = g});
				
			var extractedTable = new TabularListOfArrays(extractedTableName, new [] { "Id" , extractedColumnName}, grouped.Select(x => new object [] { x.id, x.element }));
			
			var linkTable = new TabularListOfArrays( 
				$"{t.Name}_{extractedTableName}",
				new [] {$"{t.Name}Id", $"{extractedTableName}Id"},
				grouped.SelectMany(g => g.rows.Select(r => new object [] { r.rowId, g.id })));
				
			return new NormalizationMNResult(extractedTable, linkTable);
			
		}
	}
}
