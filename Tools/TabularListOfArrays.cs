using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using QueryStorm.Data;

namespace Windy.TableTools
{
	// todo: premjestiti u QueryStorm.Data
	// todo: napraviti extension metode .From(listObject / [csv params] / [list of arrays params]) za svaki tip tabulara
	//  	tako da imamo jednu centralnu tocku odakle mozemo stvoriti bilo kakav tabular
	// todo: napraviti TabularView koji wrapa neki drugi tabular i nudi mogucnosti brisanja kolona i dodavanja kalk kolona

    class TabularListOfArrays : Tabular
    {
        private readonly IEnumerable<string> columnNames;
        private readonly IEnumerable<IEnumerable<object>> data;
        string name;

        public TabularListOfArrays(string name, IEnumerable<string> columnNames, IEnumerable<IEnumerable<object>> data)
        {
            this.name = name;
            this.columnNames = columnNames;
            this.data = data;
        }

        public override string Name => name;

        protected override void DoSaveChanges()
        {
            throw new NotImplementedException();
        }

        protected override DataSnapshot ReadData()
        {
            return new DataSnapshot(
                columnNames.Select(cn => new TabularDataColumn(this, cn, typeof(object), false, false)),
                data.Select((r, i) => new RowFromArray(i, RowStatus.Unchanged, r.ToArray(), this)));
        }

        protected override Task<DataSnapshot> ReadDataAsync(CancellationToken cancellationToken)
            => Task.FromResult(ReadData());
    }
}
