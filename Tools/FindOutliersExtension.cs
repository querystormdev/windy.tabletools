using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QueryStorm.Apps;
using QueryStorm.Data;
using QueryStorm.Tools;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
    public static class FindOutliersExtension
    {
        public static Tabular FindOutliers(this Tabular t, string columnName, string groupBy, double minDeviation)
        {
            var data = t.GroupBy(r => groupBy == null ? null : r[groupBy])
                .Select(g => new
                {
                    stdev = StandardDeviation(g.Select(r => r[columnName]).OfType<double>()),
                    avg = g.Average(r => (double)r[columnName]),
                    rows = g.ToList()
                })
                .SelectMany(g =>
                {
                	var rows = g.rows.Where(e => Math.Abs((double)e[columnName] - g.avg)  > minDeviation * g.stdev);
                    
                    return rows.Select(r => 
                    { 
                    	var columnsData = t.Columns.Select(c => c.Name == "__address" ? $"=Hyperlink(\"#{r[c]}\", \"goto\")" : r[c]);
                    	return new object[] { g.stdev, g.avg, ((double)r[columnName] - g.avg) / g.stdev }.Concat(columnsData); 
                    });
                });

            var columns = new[] { "group_stdev", "group_avg", "stdev_diff" }.Concat(t.Columns.Select(c => c.Name));

            return new TabularListOfArrays($"{t.Name}_outliers", columns, data);
        }

        public static double StandardDeviation(IEnumerable<double> values)
        {
            double avg = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v - avg, 2)));
        }
    }
}
