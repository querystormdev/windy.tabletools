using System;
using System.Collections.Generic;
using System.Linq;

namespace Windy.TableTools
{
    class ArrayValuesComparer : IEqualityComparer<IEnumerable<object>>
    {
        public bool Equals(IEnumerable<object> x, IEnumerable<object> y)
        {
            return Object.ReferenceEquals(x, y) || (x != null && y != null && x.SequenceEqual(y));
        }

        public int GetHashCode(IEnumerable<object> obj)
        {
            // Will not throw an OverflowException
            unchecked
            {
                return obj.Where(e => e != null).Select(e => e.GetHashCode()).Aggregate(17, (a, b) => 23 * a + b);
            }
        }
    }
}
