namespace Windy.TableTools
{
    public enum DetailsTableDestination
    {
        ReplaceOriginalTable,
        InNewSheet,
        ManualLocation 
    }
}
