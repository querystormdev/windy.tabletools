using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Humanizer;
using QueryStorm.Apps;
using QueryStorm.Apps.Contract;
using QueryStorm.Data;
using QueryStorm.Tools;
using QueryStorm.Tools.Excel;
using Unity;
using Unity.Lifetime;
using Windy.TableTools.Commands;
using static QueryStorm.Tools.DebugHelpers;

namespace Windy.TableTools
{
    public class App : ApplicationModule
    {
        const string ContextMenuTitle = "Table Tools";
        const string ContextMenuTag = "Windy.TableTools";

        public App(IAppHost appHost)
            : base(appHost)
        {
        	// replace with dialog service after moving the method there
            Container.RegisterType<WindowService>(new ContainerControlledLifetimeManager());
        }
    }
}